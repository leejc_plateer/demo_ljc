package com.demo.utils;

public enum Gender {
	MALE("male"),
	FEMALE("female");
	
	private String string;
	
	private Gender(String string) {
		this.string = string;
	}
	
	public String getString() {
		return this.string;
	}
}//test4 test5 test6 test7
